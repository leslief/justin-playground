﻿/*********  Full-Width Slider *****************************/
//$('#fw-slider > .frame').mightySlider ({
//	viewport:'fill'
//});
//********  Currently not being used but can be added in as a widget later *****//			
(function () {
    var $example = $('#fw-slider'),
	$frame = $('.frame', $example);
    $caption = $('#caption2', $example),
    //$count = $('#count', $example);
	$frame.mightySlider({
	    //load: getHeight(),
	    speed: 1000,
	    viewport: 'fill',
	    easing: 'easeOutExpo',
	    // Navigation options
	    navigation: {
	        slideSize: '100%',
	        keyboardNavBy: 'slides'
	    },

	    // Deep-linking options
	    //deeplinking: {
	    //	linkID: 'what-ever-you-want-here'
	    //},

	    // Dragging
	    dragging: {
	        swingSpeed: 0.1
	    },

	    // Commands
	    commands: {
	        pages: 1,
	        buttons: 1
	    }
	},
	{
	    active: function (name, index) {
	        $caption.html(this.slides[index].options.caption);
	        //$count.html((index + 1) + '/' + this.slides.length + '&nbsp;&nbsp;&nbsp;&nbsp;' + this.slides[index].options.name);
	    }
	});
})();