﻿MarcusApps = function() {};
MarcusApps.Packages = function () {
};

MarcusApps.Packages.prototype = {};

MarcusApps.Packages.DisplayBanner = function (elem) {
    $.ajax({
        method: 'get',
        url: '/packages/banner',
        data: { categoryName: $(elem).data("category") },
        success: function (data) {
            $(elem).html(data);
        }
    });
};

$(document).ready(function () {
    $(".package-banner").each(function (i, e) {
        MarcusApps.Packages.DisplayBanner(e);
    });
});