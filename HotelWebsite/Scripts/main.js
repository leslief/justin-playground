//Main JS file to be used for all pages.

// Primary Navigation Modification 
$(document).ready(function () {
    // Hacking the nav to work outside original design
    $('.nav-btn').bind('click', function () {
        /*var nav = $('#nav');
        var height = $(window).height();
        var newHeight = height - 92;
       
        if ((nav.hasClass('not-open'))) {
            nav.removeClass('not-open');
            nav.addClass('open');
            nav.css('min-height', newHeight);
        }
        else if (nav.hasClass('open')) {
            nav.removeClass('open');
            nav.addClass('not-open');
        }*/
    });

    $(window).load(function () {
        enquire.register("screen and (min-width: 768px)", {
            setup: function () {
                var nav = $('#nav');
            },
            match: function () {
                var navItem = $('#nav').detach();
                $('.utility-nav--desktop').after(navItem);
            },
            unmatch: function () {
                var navItem = $('#nav').detach();
                $('.header').after(navItem);
            }
        });
    });

});

// Sticky Header 
$('#header').sticky({ topSpacing: 0 });

// Equal Height for featured content columns
equalheight = function (t) {
    var winWidth = $(window).width();
    if (winWidth > 1020) {
        var e, i = 0, n = 0, h = new Array;
        $(t).each(function () {
            if (e = $(this), $(e).height("auto"), topPostion = e.position().top, n != topPostion) {
                for (currentDiv = 0; currentDiv < h.length; currentDiv++)
                    h[currentDiv].height(i);
                h.length = 0, n = topPostion, i = e.height(), h.push(e)
            } else
                h.push(e), i = i < e.height() ? e.height() : i;
            for (currentDiv = 0; currentDiv < h.length; currentDiv++)
                h[currentDiv].height(i + 50)
        })
    }
    else {
        var e, i = 0, n = 0, h = new Array;
        $(t).each(function () {
            if (e = $(this), $(e).height("auto"), topPostion = e.position().top, n != topPostion) {
                for (currentDiv = 0; currentDiv < h.length; currentDiv++)
                    h[currentDiv].height("auto");
                h.length = 0, n = topPostion, i = e.height(), h.push(e)
            } else
                h.push(e), i = i < e.height() ? e.height() : i;
            for (currentDiv = 0; currentDiv < h.length; currentDiv++)
                h[currentDiv].height("auto");
        })
    }

}, $(window).load(function () {
    //equalheight(".featured__content")
}), $(window).resize(function () {
    //equalheight(".featured__content")
});


// Search function to toggle input.
$('.ut-nav--search > a').click(function () {
    $('.ut-nav--search > a').css('display', 'none');
    var effect = "slide";
    var options = "down";
    var duration = 500;
    $('.ut-nav--search').css('width', '241');
    //$('.search-box').toggle(duration);
    $('.search-box').css('display', 'block');
});

/**** Full Width Slider ***/
/*********  Full-Width Slider *****************************/
//$('#fw-slider > .frame').mightySlider ({
//	viewport:'fill'
//});

(function () {
    var $example = $('#fw-slider'),
	$frame = $('.frame', $example);
    $caption = $('#caption2', $example),
    $button = $('#callout-button', $example);
    //$count = $('#count', $example);
	$frame.mightySlider({
	    //load: getHeight(),
	    speed: 1000,
	    viewport: 'fill',
	    easing: 'easeOutExpo',
	    // Navigation options
	    navigation: {
	        slideSize: '100%',
	        keyboardNavBy: 'slides'
	    },

	    // Deep-linking options
	    //deeplinking: {
	    //	linkID: 'what-ever-you-want-here'
	    //},

	    // Dragging
	    dragging: {
	        swingSpeed: 0.1
	    },

	    // Commands
	    commands: {
	        pages: 1,
	        buttons: 1
	    }
	},
	{
	    active: function (name, index) {
	        $caption.html(this.slides[index].options.caption);
	        $button.html(this.slides[index].options.name);
	        //$count.html((index + 1) + '/' + this.slides.length + '&nbsp;&nbsp;&nbsp;&nbsp;' + this.slides[index].options.name);
	    }
	});
})();

/*********  Image Gallery Slider *****************************/
			
(function(){
	var $example = $('#media-slider'),
	$frame = $('.frame', $example);
	//$caption = $('#caption', $example),
	$count = $('#pic-count', $example);
	$frame.mightySlider({
		//load: getHeight(),
		speed: 1000,
		//viewport: 'fill',
		easing: 'easeOutExpo',
		// Navigation options
		navigation: {
			slideSize: '100%',
			keyboardNavBy: 'slides'
		},
		
		// Deep-linking options
		//deeplinking: {
		//	linkID: 'what-ever-you-want-here-2'
		//},

		// Dragging
		dragging: {
			swingSpeed:    0.1
		},

		// Commands
		commands: {
			thumbnails:1
		}
	},
	{
		active: function(name, index){
		//$caption.html(this.slides[index].options.caption);
		$count.html((index + 1) + '/' + this.slides.length);
	}
	});
})();		


/******************* Interior Page Slider ************************************/

/***********************************  Full-Page Slider ****************************/

(function(){
	var $example = $('#interior-slider'),
	$frame = $('.frame', $example),
	$caption = $('#caption', $example),
	$count = $('#count', $example);
	$frame.mightySlider({
		speed: 1000,
		moveBy: 300,
		viewport: 'fill',
		easing: 'easeOutExpo',
		
		// Navigation options
		navigation: {
			slideSize: '100%',
			keyboardNavBy: 'slides'
		},
		
		// Deep-linking options
		//deeplinking: {
		//	linkID: 'interior-page-slider'
		//},

		// Dragging
		dragging: {
			swingSpeed:    0.1
		},

		// Commands
		commands: {
			buttons: 1
		}
	},
	{
		active: function(name, index){
		$caption.html(this.slides[index].options.caption);
		$count.html((index + 1) + '/' + this.slides.length + '&nbsp;&nbsp;&nbsp;&nbsp;' + this.slides[index].options.name);
	}
	});
})();



/* Touch Interface for Primary Navigation */
if ($('html').hasClass('touch')) {
    var clicks = true;
    var touchOutside = false;
   
    $('html').on('click', function (event) {
        //Handles Touch outside of the navigation without use of preventDefault
        var event = $(event.target).closest('#nav').length;

        if (event != 1) {
            $('.nav__list--secondary').removeClass('hovered');
        }
    });

  /*  function navMenu(e, clickItem) {
        var subNav = (clickItem.next('.nav__list--secondary'));

        if (clicks) {
            subNav.addClass('hovered');
               
            clicks = false;
            return false;
        }
    }*/
    
    function navMenuTouch(e, clickItem) {
        var subNav = (clickItem.next('.nav__list--secondary'));
        $('.nav__item--primary').find('ul').removeClass('hovered');
        if (!(subNav.hasClass('hovered'))) {
            subNav.addClass('hovered');
        }
    }
    // Working on this... this works for chrome 
    $('.nav__item--primary > a').on('click', function (e) {
        var clickItem = $(this);
        navMenuTouch(e, clickItem);
    
    });

    /*$('.nav__item--primary > a').off('click touchstart MSPointerDown', function (e) {
        subNav.removeClass('hovered');
    });*/
    
    /*if (clicks) {
        $('.nav__item--primary > a').on('click touchstart MSPointerDown', function (e) {
            var clickItem = $(this);
            navMenu(e, clickItem);

        });
    }
    else {
        $('.nav__item--primary > a').off('click touchstart MSPointerDown', function (e) {
            subNav.removeClass('hovered');
            clicks = true;
        });
    }*/

    // Create toggle for touch devices on mobile for primary nav item
    /*$(window).load(function () {
        enquire.register("screen and (min-width: 768px)", {
            setup: function () {  
            },
            match: function () {
                $('li.primary-nav__item > a').attr('href', 'javascript:void(0)');
            },
            unmatch: function () {
                $('li.primary-nav__item > a').attr('href', 'javascript:void(0)');
            }
        });
    });*/ 
}

/*********** Side Navigation Interaction *************/
//Open Current Item on page load
$('.aside-nav__item.active.open.hasChild').children('ul.aside-nav__list--secondary').addClass('effectFadeInDown');

//Click interaction
$('.aside-nav__item.hasChild > a').click(function () {
    var parentLI = $(this).parent('li');
    sideNavInteraction(parentLI);
});

function sideNavInteraction(pl) {
    //event.preventDefault();
    var subNav = (pl.children('ul.aside-nav__list--secondary'));

    if (pl.hasClass('open')) {
        $('ul.aside-nav__list--secondary').removeClass('effectFadeInDown').css('position', 'absolute');
        $('.aside-nav__item').removeClass('open');
        subNav.removeClass('effectFadeInDown').css('position', 'absolute');
    }
    else {
        $('ul.aside-nav__list--secondary').removeClass('effectFadeInDown').css('position', 'absolute');
        subNav.addClass('effectFadeInDown').css('position', 'relative');
        $('.aside-nav__item').removeClass('open');
        pl.addClass('open');
    }
};

$('.btn--roomDetails').bind('click', function() {
	var test = $(this).parent().parent().parent().next('div.room-details');
	var roomDetail = $(this).closest('div.room-overview').next('div.room-details');
	roomDetail.slideToggle();
});

$('.btn--reservation').bind('click', function (e) {
    $('#resForm').slideToggle();
});

/* Make a Reservation Button */
$("#datePicker-Res").datepicker({
    altField: "#PickupDate",
    minDate: new Date()
});

/* Read More button for long text */
/*$('.trunc-text').readmore({
    speed: 75,
    maxHeight: 200,
    moreLink: '<a href="#" class="read-more__link">Read more...</a>',
    lessLink: '<a href="#" class="read-more__link">Close</a>'
});*/

//Lazy Load Images
$("img.lazy").show().lazyload({
    //effect: "fadein"
});


// Mobile Nav Button 
$('#nav-open-btn').on('click', function () {
    //var navStatus = $('#nav').hasClass('open');
    //var navStatus2 = $('#open-menu-image').hasClass('no-show');
    //var offSetTop = $('#nav').offset().top;
   /* if (!(navStatus)) {
        $('#open-menu-image').addClass('no-show');
        $('#close-menu-image').removeClass('no-show');
    }
    else {
        $('#close-menu-image').addClass('no-show');
        $('#open-menu-image').removeClass('no-show');
    }*/
});

// Mobile Scroll to Top
/* scrollUp Minimum setup */
$(function () {
    $.scrollUp();
});
 
/* scrollUp full options
$(function () {
    $.scrollUp({
        scrollName: 'scrollUp', // Element ID
        topDistance: '300', // Distance from top before showing element (px)
        topSpeed: 300, // Speed back to top (ms)
        animation: 'fade', // Fade, slide, none
        animationInSpeed: 200, // <a href="http://www.jqueryscript.net/animation/">Animation</a> in speed (ms)
        animationOutSpeed: 200, // Animation out speed (ms)
        scrollText: 'Scroll to top', // Text for element
        activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
    });
});
*/
 
/*$('a.ui-state-default').bind('click', function () {
    console.log('click');
    var day = $(this).html();
    var parentItem = $(this).parent();
    var month = parentItem.attr('data-month');
    var year = parentItem.attr('data-year');
    console.log(day, month, year);
});*/

/* Add datepicker to all input types that are date in non-supported browsers */
if (!(Modernizr.inputtypes.date)) {
    $("input[type=date]").datepicker({
        dateFormat: 'mm-dd-yy',
        minDate: new Date()
    });
}

/* Pop-up Modal Photo Gallery */
$('a.fancy').colorbox({
    maxWidth: "80%",
});

/* Open external links in a blank window */
$("a[href^='http://']").attr("target", "_blank");
$("a[href^='https://']").attr("target", "_blank");


/* HANDLES THUMBNAILS ON PACKAGES AND EVENTS LISTINGS */
var $container = $('.packages').imagesLoaded(function () {
    $container.isotope({
        itemSelector: '.item',
        layoutMode: 'packery'
    });
});

var $container1 = $('.packages').imagesLoaded(function () {
    $container1.isotope({
        itemSelector: '.element-item',
        layoutMode: 'packery'
    });
});


$('#filters').on('click', 'button', function () {
    var sortValue = $(this).attr('data-filter');
    console.log(sortValue);
    $container.isotope({ filter: sortValue });
});

// change is-checked class on buttons
$('.filter__group').each(function (i, buttonGroup) {
    var $buttonGroup = $(buttonGroup);
    $buttonGroup.on('click', 'button', function () {
        $buttonGroup.find('.is-checked').removeClass('is-checked');
        $(this).addClass('is-checked');
    });
});