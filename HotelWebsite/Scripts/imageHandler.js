﻿$(window).load(function () {
    if ($('.page-header__image').length)
    {
        var srcImgOrig = $('.page-header__image').attr('src');
        var srcImgArray = $('.page-header__image').attr('src').split('?');
        var srcImg = srcImgArray[0];
        //var srcImg2 = $('.small-header__image').attr('src');
        enquire.register("screen and (min-width: 480px)", {
            setup: function () {
                var small = '?w=1054&h=800&mode=crop';
                $('.page-header__image').attr('src', srcImg + small);
                //      $('.small-header__image').attr('src', srcImg2 + small);
            },
            match: function () {
                $('.page-header__image').attr('src', srcImgOrig);
                //    $('.small-header__image').attr('src', srcImg2 + '?w=2000');
            },
            unmatch: function () {
                var small = '?w=1054&h=800&mode=crop';
                $('.page-header__image').attr('src', srcImg + small);
                //  $('.small-header__image').attr('src', srcImg + small);
            }
        });
    }
});

if (!Modernizr.svg) {
    $('img[src*="svg"').attr('src', function () {
        return $(this).attr('src').replace('.svg', '.png');
    });
}