﻿namespace HotelWebsite.Helpers
{
    public static class StringExtensions
    {
        public static string CleanChar(this string buffer)
        {
            if (buffer.IndexOf('\u2013') > -1) buffer = buffer.Replace('\u2013', '-');
            if (buffer.IndexOf('\u2014') > -1) buffer = buffer.Replace('\u2014', '-');
            if (buffer.IndexOf('\u2015') > -1) buffer = buffer.Replace('\u2015', '-');
            if (buffer.IndexOf('\u2017') > -1) buffer = buffer.Replace('\u2017', '_');
            if (buffer.IndexOf('\u2018') > -1) buffer = buffer.Replace('\u2018', '\'');
            if (buffer.IndexOf('\u2019') > -1) buffer = buffer.Replace('\u2019', '\'');
            if (buffer.IndexOf('\u201a') > -1) buffer = buffer.Replace('\u201a', ',');
            if (buffer.IndexOf('\u201b') > -1) buffer = buffer.Replace('\u201b', '\'');
            if (buffer.IndexOf('\u201c') > -1) buffer = buffer.Replace('\u201c', '\"');
            if (buffer.IndexOf('\u201d') > -1) buffer = buffer.Replace('\u201d', '\"');
            if (buffer.IndexOf('\u201e') > -1) buffer = buffer.Replace('\u201e', '\"');
            if (buffer.IndexOf('\u2026') > -1) buffer = buffer.Replace("\u2026", "...");
            if (buffer.IndexOf('\u2032') > -1) buffer = buffer.Replace('\u2032', '\'');
            if (buffer.IndexOf('\u2033') > -1) buffer = buffer.Replace('\u2033', '\"');

            if(buffer.IndexOf(System.Environment.NewLine, System.StringComparison.Ordinal) > -1) buffer = buffer.Replace(System.Environment.NewLine, " ");

            return buffer;
        }
    }
}