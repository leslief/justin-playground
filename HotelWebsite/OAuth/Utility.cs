﻿using System.IO;
using System.Net;

namespace HotelWebsite.OAuth
{
    public class Utility
    {
        public string RequstJson(string apiUrl, string tokenType, string accessToken)
        {
            const string timelineHeaderFormat = "{0} {1}";
            string response;
            var apiRequest = (HttpWebRequest)WebRequest.Create(apiUrl);            
            
            apiRequest.Headers.Add("Authorization",
                                        string.Format(timelineHeaderFormat, tokenType,
                                                      accessToken));
            apiRequest.Method = "Get";
            
            var timeLineResponse = apiRequest.GetResponse();

            using (timeLineResponse)
            {
                using (var reader = new StreamReader(timeLineResponse.GetResponseStream()))
                {
                    response = reader.ReadToEnd();
                }
            }

            return response;
        }
    }
}