﻿using System;
using System.Configuration;

namespace HotelWebsite.OAuth.Twitter.Services.Implamentations
{
    public class OAuthTwitterWrapper : IOAuthTwitterWrapper
    {
        public IAuthenticateSettings AuthenticateSettings { get; set; }
        public ITimeLineSettings TimeLineSettings { get; set; }
        public ISearchSettings SearchSettings { get; set; }

        /// <summary>
        /// The default constructor takes all the settings from the appsettings file
        /// </summary>
        public OAuthTwitterWrapper()
        {
            var oAuthConsumerKey = ConfigurationManager.AppSettings["oAuthConsumerKey"];
            var oAuthConsumerSecret = ConfigurationManager.AppSettings["oAuthConsumerSecret"];
            var oAuthUrl = ConfigurationManager.AppSettings["oAuthUrl"];

            AuthenticateSettings = new AuthenticateSettings { OAuthConsumerKey = oAuthConsumerKey, OAuthConsumerSecret = oAuthConsumerSecret, OAuthUrl = oAuthUrl };            
        }

        /// <summary>
        /// This allows the authentications settings to be passed in
        /// </summary>
        /// <param name="authenticateSettings"></param>
        public OAuthTwitterWrapper(IAuthenticateSettings authenticateSettings)
        {
            AuthenticateSettings = authenticateSettings;
        }

        /// <summary>
        /// This allows the authentications and timeline settings to be passed in
        /// </summary>
        /// <param name="authenticateSettings"></param>
        /// <param name="timeLineSettings"></param>
        public OAuthTwitterWrapper(IAuthenticateSettings authenticateSettings, ITimeLineSettings timeLineSettings)
        {
            AuthenticateSettings = authenticateSettings;
            TimeLineSettings = timeLineSettings;
        }

        /// <summary>
        /// This allows the authentications, timeline and search settings to be passed in
        /// </summary>
        /// <param name="authenticateSettings"></param>
        /// <param name="timeLineSettings"></param>
        /// <param name="searchSettings"></param>
        public OAuthTwitterWrapper(IAuthenticateSettings authenticateSettings, ITimeLineSettings timeLineSettings, ISearchSettings searchSettings)
        {
            AuthenticateSettings = authenticateSettings;
            TimeLineSettings = timeLineSettings;
            SearchSettings = searchSettings;
        }

        public string GetMyTimeline()
        {
            IAuthenticate authenticate = new Authenticate();
            var twitAuthResponse = authenticate.AuthenticateMe(AuthenticateSettings);

            // Do the timeline
            var utility = new Utility();
            var timeLineJson = utility.RequstJson(TimeLineSettings.TimelineUrl, twitAuthResponse.TokenType, twitAuthResponse.AccessToken);

            return timeLineJson;
        }

        public string GetSearch()
        {
            IAuthenticate authenticate = new Authenticate();
            var twitAuthResponse = authenticate.AuthenticateMe(AuthenticateSettings);

            // Do the timeline
            var utility = new Utility();
            var searchJson = utility.RequstJson(SearchSettings.SearchUrl, twitAuthResponse.TokenType, twitAuthResponse.AccessToken);

            return searchJson;
        }

        public void SetTimeLineSettings(TimeLineSettings settings)
        {
            TimeLineSettings = settings;
        }

        public void SetSearchSetting(SearchSettings settings)
        {
            SearchSettings = settings;
        }
    }
}