﻿using System;
using System.IO;
using System.Net;
using System.Text;
using HotelWebsite.OAuth.Twitter.JsonTypes;
using Newtonsoft.Json;

namespace HotelWebsite.OAuth.Twitter.Services.Implamentations
{
    public class Authenticate : IAuthenticate
    {
        public AuthResponse AuthenticateMe(IAuthenticateSettings authenticateSettings)
        {
            AuthResponse twitAuthResponse;

            // Do the Authenticate
            const string authHeaderFormat = "Basic {0}";
            const string postBody = "grant_type=client_credentials";

            var authHeader = string.Format(authHeaderFormat,
                                           Convert.ToBase64String(
                                               Encoding.UTF8.GetBytes(Uri.EscapeDataString(authenticateSettings.OAuthConsumerKey) + ":" +

                                                                      Uri.EscapeDataString((authenticateSettings.OAuthConsumerSecret)))

                                               ));
            
            var authRequest = (HttpWebRequest)WebRequest.Create(authenticateSettings.OAuthUrl);

            authRequest.Headers.Add("Authorization", authHeader);
            authRequest.Method = "POST";
            authRequest.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";
            authRequest.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            
            using (var stream = authRequest.GetRequestStream())
            {
                var content = Encoding.ASCII.GetBytes(postBody);
                stream.Write(content, 0, content.Length);
            }
            
            authRequest.Headers.Add("Accept-Encoding", "gzip");

            var authResponse = authRequest.GetResponse();

            // deserialize into an object
            using (authResponse)
            {
                using (var reader = new StreamReader(authResponse.GetResponseStream()))
                {
                    var objectText = reader.ReadToEnd();
                    twitAuthResponse = JsonConvert.DeserializeObject<AuthResponse>(objectText);
                }
            }

            return twitAuthResponse;
        }
    }
}