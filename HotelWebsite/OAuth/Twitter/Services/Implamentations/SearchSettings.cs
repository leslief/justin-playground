﻿namespace HotelWebsite.OAuth.Twitter.Services.Implamentations
{
    public class SearchSettings : ISearchSettings
    {
        public string SearchFormat { get; set; }
        public string SearchQuery { get; set; }
        public string SearchUrl
        {
            get { return string.Format(SearchFormat, SearchQuery); }
        }
    }
}