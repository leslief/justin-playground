﻿using HotelWebsite.OAuth.Twitter.Services.Implamentations;

namespace HotelWebsite.OAuth.Twitter.Services
{
    public interface IOAuthTwitterWrapper
    {
        string GetMyTimeline();
        string GetSearch();

        void SetTimeLineSettings(TimeLineSettings settings);
        void SetSearchSetting(SearchSettings settings);
    }
}
