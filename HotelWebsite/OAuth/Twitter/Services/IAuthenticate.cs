﻿using HotelWebsite.OAuth.Twitter.JsonTypes;

namespace HotelWebsite.OAuth.Twitter.Services
{
    public interface IAuthenticate
    {
        AuthResponse AuthenticateMe(IAuthenticateSettings authenticateSettings);
    }
}
