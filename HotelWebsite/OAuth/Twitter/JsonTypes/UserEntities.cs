﻿using Newtonsoft.Json;

namespace HotelWebsite.OAuth.Twitter.JsonTypes
{
    public class UserEntities
    {
        [JsonProperty("description")]
        public Description Description { get; set; }
    }
}