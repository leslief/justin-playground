﻿using Newtonsoft.Json;

namespace HotelWebsite.OAuth.Twitter.JsonTypes
{
    public class AuthResponse
    {
        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
    }
}