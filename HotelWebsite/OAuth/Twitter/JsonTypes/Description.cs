﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace HotelWebsite.OAuth.Twitter.JsonTypes
{
    public class Description
    {
        [JsonProperty("urls")]
        public List<Url> Urls { get; set; }
    }
}