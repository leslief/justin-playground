﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace HotelWebsite.Models.Events
{
    [DataContract]
    public class EventModel
    {
        private const string DateFormat = "ddd, dd MMM yyyy HH:mm:ss";

        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "teaser")]
        public string Teaser { get; set; }

        [DataMember(Name = "description")]
        public string Description { get; set; }

        public DateTime DisplayOn { get; set; }

        [DataMember(Name = "featured")]
        public bool IsFeatured { get; set; }

        [DataMember(Name = "disabled")]
        public bool IsDisabled { get; set; }

        [DataMember(Name = "allDay")]
        public bool AllDay
        {
            get { return StartOn.Hour == 0; }
            protected set { }
        }
        public DateTime StartOn { get; set; }

        [DataMember(Name = "start")]
        public string FormattedStart
        {
            get { return StartOn.ToString(DateFormat); }
            set { StartOn = Convert.ToDateTime(value); }
        }
        public DateTime EndOn { get; set; }

        [DataMember(Name = "end")]
        public string FormattedEnd
        {
            get { return EndOn.ToString(DateFormat); }
            set { EndOn = Convert.ToDateTime(value); }
        }

        [DataMember(Name = "descriptiveTime")]
        public string DescriptiveTime { get; set; }

        #region Location

        [DataMember(Name = "location")]
        public string LocationName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }

        public string PhoneNumber { get; set; }

        #endregion

        #region Media/other

        [DataMember(Name = "url")]
        public string WebsiteUrl { get; set; }
        public string BookingUrl { get; set; }

        public int Priority { get; set; }

        public string ImageFilename { get; set; }
        [DataMember(Name = "imageUrl")]
        public string ImageUrl { get; set; }
        public string ThumbnailFilename { get; set; }
        [DataMember(Name = "thumbnailUrl")]
        public string ThumbnailUrl { get; set; }

        #endregion

        [DataMember(Name = "categories")]
        public IList<CategoryModel> Categories { get; set; }

        [DataMember(Name = "updatedOn")]
        public DateTime UpdatedOn { get; set; }

        public EventModel()
        {
            Categories = new List<CategoryModel>();
        }
    }
}