﻿using System.Collections.Generic;

namespace HotelWebsite.Models.Events
{
    public class EventListingModel
    {
        private const string DateFormat = "ddd, dd MMM yyyy HH:mm:ss";

        public IEnumerable<EventModel> Events { get; set; }

        public IEnumerable<EventModel> RecurringEvents { get; set; }

        public string StartDate { get; set; }

        public string EndDate { get; set; }

        public EventListingModel()
        {
            Events = new List<EventModel>();
            RecurringEvents = new List<EventModel>();
        }
    }
}