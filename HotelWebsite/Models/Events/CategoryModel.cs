﻿using System;
using System.Runtime.Serialization;

namespace HotelWebsite.Models.Events
{
    [DataContract]
    public class CategoryModel
    {
        [DataMember(Name = "id")]
        public Guid Id { get; set; }
        
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "color")]
        public string Color { get; set; }
    }
}