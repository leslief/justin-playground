﻿using System;
using System.Collections.Generic;

namespace HotelWebsite.Models.Media
{
    public class AlbumModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int SortOrder { get; set; }
        public bool IsDisabled { get; set; }
        public bool IsHidden { get; set; }

        public DateTime UpdatedOn { get; set; }

        public int StartingIndex { get; set; }

        public string CoverPhotoUrl { get; set; }
        public IEnumerable<PhotoModel> Photos { get; set; }
    }
}