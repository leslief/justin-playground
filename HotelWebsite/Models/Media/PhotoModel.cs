﻿using System;

namespace HotelWebsite.Models.Media
{
    public class PhotoModel
    {
        public Guid Id { get; set; }
        public AlbumModel Album { get; set; }
        public string Filename { get; set; }
        public string ImageUrl { get; set; }
        public string Caption { get; set; }
        public string AltText { get; set; }
        public int SortOrder { get; set; }
        public bool IsDisabled { get; set; }
    }
}