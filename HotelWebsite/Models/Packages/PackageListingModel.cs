﻿using System.Collections.Generic;

namespace HotelWebsite.Models.Packages
{
    public class PackageListingModel
    {
        public IEnumerable<PackageModel> Packages { get; set; }
        public IEnumerable<CategoryModel> Categories { get; set; }
    }
}