﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace HotelWebsite.Models.Packages
{
    [DataContract]
    public class PackageModel
    {
        [DataMember(Name = "id")]
        public Guid? Id { get; set; }

        [DataMember(Name = "title")]
        public string Title { get; set; }

        [DataMember(Name = "teaser")]
        public string Teaser { get; set; }
        [DataMember(Name = "description")]
        public string Description { get; set; }

        [DataMember(Name = "displayFrom")]
        public DateTime DisplayFrom { get; set; }

        [DataMember(Name = "displayTo")]
        public DateTime? DisplayTo { get; set; }

        [DataMember(Name = "isHidden")]
        public bool IsHidden { get; set; }

        [DataMember(Name = "isDisabled")]
        public bool IsDisabled { get; set; }

        [DataMember(Name = "price")]
        public decimal? Price { get; set; }

        [DataMember(Name = "priceDetails")]
        public string PriceDetails { get; set; }

        [DataMember(Name = "priceHidden")]
        public bool PriceIsHidden { get; set; }

        [DataMember(Name = "restrictions")]
        public string Restrictions { get; set; }

        [DataMember(Name = "categories")]
        public IEnumerable<CategoryModel> Categories { get; set; }

        #region Booking info

        [DataMember(Name = "bookingMethod")]
        public string BookingMethod { get; set; }
        
        [DataMember(Name = "bookingPhone")]
        public string BookingPhone { get; set; }

        [DataMember(Name = "codeType")]
        public string CodeType { get; set; }
        
        [DataMember(Name = "code")]
        public string Code { get; set; }

        [DataMember(Name = "bookingUrl")]
        public string BookingUrl { get; set; }

        #endregion

        #region Ad Settings

        [DataMember(Name = "adType")]
        public string AdType { get; set; }

        [DataMember(Name = "largeImageFilename")]
        public string LargeImageFilename { get; set; }

        [DataMember(Name = "smallImageFilename")]
        public string SmallImageFilename { get; set; }

        [DataMember(Name = "backgroundImageFilename")]
        public string BackgroundImageFilename { get; set; }
       
        [DataMember(Name = "bannerImageFilename")]
        public string BannerImageFilename { get; set; }
        
        [DataMember(Name = "bannerTitle")]
        public string BannerTitle { get; set; }
        
        [DataMember(Name = "bannerText")]
        public string BannerText { get; set; }

        #endregion

        #region SEO
        [DataMember(Name = "pageTitle")]
        public string PageTitle { get; set; }
        
        [DataMember(Name = "metaDescription")]
        public string MetaDescription { get; set; }
        #endregion

        public IEnumerable<PackageModel> RelatedPackages { get; set; }

        [DataMember(Name = "updatedOn")]
        public DateTime UpdatedOn { get; set; }

        public PackageModel()
        {
            RelatedPackages = new List<PackageModel>();
            Categories = new List<CategoryModel>();
        }
    }
}