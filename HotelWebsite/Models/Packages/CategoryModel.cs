﻿using System;
using System.Runtime.Serialization;

namespace HotelWebsite.Models.Packages
{
    [DataContract]
    public class CategoryModel : IEquatable<CategoryModel>
    {
        [DataMember(Name = "id")]
        public Guid Id { get; set; }
        
        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "isDisabled")]
        public bool IsDisabled { get; set; }

        public bool Equals(CategoryModel other)
        {
            if (ReferenceEquals(other, null)) return false;
            if (ReferenceEquals(this, other)) return true;
            return Id.Equals(other.Id) && Name.Equals(other.Name);
        }

        public override int GetHashCode()
        {
            return Name == null ? 0 : Name.GetHashCode() ^
                    IsDisabled.GetHashCode() ^
                  Id.GetHashCode() ;
        }
    }
}