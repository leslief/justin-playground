﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace HotelWebsite.Models.Forms
{
    public class ContactModel
    {
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string EmailAddress { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string GroupName { get; set; }
        public string File { get; set; }
        public DateTime? BookBy { get; set; }
        public int GroupSize { get; set; }
        public string RateExpected { get; set; }
        public int RoomsPerNight { get; set; }
        public int RoomsPeakNight { get; set; }
        public bool MeetingSpace { get; set; }
        public DateTime? FirstDay { get; set; }
        public DateTime? LastDay { get; set; }
        public DateTime? FirstDayMeeting { get; set; }
        public DateTime? LastDayMeeting { get; set; }
        public string EventType { get; set; }
        public int NumberOfPeople { get; set; }
        public string Setup { get; set; }
        public string Comments { get; set; }
        public int BreakoutRooms { get; set; }
        public string BirthdayMonth { get; set; }
        public string BirthdayDay { get; set; }
        public string AnniversaryMonth { get; set; }
        public string AnniversaryDay { get; set; }
        public bool Spa { get; set; }
        public bool Dining { get; set; }
        public bool Holidays { get; set; }
        public List<SelectListItem> AvailableStates { get; set; }
        public string SelectedStates { get; set; }

        public List<SelectListItem> ContactReason { get; set; }
        public string SelectedReasons { get; set; }

        public ContactModel()
        {
            ContactReason = new List<SelectListItem>
                {
                    new SelectListItem
                        {
                            Text = "Nature of Request",
                            Value = "",
                            Selected = true
                        },
                    new SelectListItem
                        {
                            Text = "Suggestions",
                            Value = "Suggestions",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Complaints",
                            Value = "Complaints",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Compliments",
                            Value = "Compliments",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Other",
                            Value = "Other",
                            Selected = false
                        }
                };

            AvailableStates = new List<SelectListItem>
                {
                    new SelectListItem
                        {
                            Text = "Alabama",
                            Value = "AL",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Alaska",
                            Value = "AK",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Arizona",
                            Value = "AZ",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Arkansas",
                            Value = "AR",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "California",
                            Value = "CA",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Colorado",
                            Value = "CO",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Connecticut",
                            Value = "CT",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Delaware",
                            Value = "DE",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "District of Columbia",
                            Value = "DC",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Florida",
                            Value = "FL",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Georgia",
                            Value = "GA",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Hawaii",
                            Value = "HI",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Idaho",
                            Value = "ID",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Illinois",
                            Value = "IL",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Indiana",
                            Value = "IN",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Iowa",
                            Value = "IA",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Kansas",
                            Value = "KS",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Kentucky",
                            Value = "KY",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Louisiana",
                            Value = "LA",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Maine",
                            Value = "ME",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Maryland",
                            Value = "MD",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Massachusetts",
                            Value = "MA",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Michigan",
                            Value = "MI",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Minnesota",
                            Value = "MN",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Mississippi",
                            Value = "MS",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Missouri",
                            Value = "MO",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Montana",
                            Value = "MT",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Nebraska",
                            Value = "NE",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "New Hampshire",
                            Value = "NH",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "New Jersey",
                            Value = "NJ",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "New Mexico",
                            Value = "NM",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "New York",
                            Value = "NY",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "North Carolina",
                            Value = "NC",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "North Dakota",
                            Value = "ND",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Ohio",
                            Value = "OH",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Oklahoma",
                            Value = "OK",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Oregon",
                            Value = "OR",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Pennsylvania",
                            Value = "PA",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Rhode Island",
                            Value = "RI",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "South Carolina",
                            Value = "SC",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "South Dakota",
                            Value = "SD",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Tennessee",
                            Value = "TN",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Texas",
                            Value = "TX",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Utah",
                            Value = "UT",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Vermont",
                            Value = "VT",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Virginia",
                            Value = "VA",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Washington",
                            Value = "WA",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "West Virginia",
                            Value = "WV",
                            Selected = false
                        },
                    new SelectListItem
                        {
                            Text = "Wisconsin",
                            Value = "WI",
                            Selected = true
                        },
                    new SelectListItem
                        {
                            Text = "Wyoming",
                            Value = "WY",
                            Selected = false
                        }
                };
        }
    }
}