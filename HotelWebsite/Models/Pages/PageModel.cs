﻿using System;
using System.Runtime.Serialization;

namespace HotelWebsite.Models.Pages
{
    [DataContract]
    public class PageModel
    {
        [DataMember(Name = "id")]
        public Guid? Id { get; set; }

        [DataMember(Name = "parentPageId")]
        public Guid? ParentPageId { get; set; }

        [DataMember(Name = "parentPageName")]
        public string ParentPageName { get; set; }

        [DataMember(Name = "parentPageUrl")]
        public string ParentPageUrl { get; set; }

        [DataMember(Name = "websiteId")]
        public Guid WebsiteId { get; set; }

        [DataMember(Name = "websiteName")]
        public string WebsiteName { get; set; }

        [DataMember(Name = "template")]
        public TemplateModel Template { get; set; }

        [DataMember(Name = "pageName")]
        public string PageName { get; set; }

        [DataMember(Name = "content")]
        public string Content { get; set; }

        [DataMember(Name = "renderedContent")]
        public string RenderedContent { get; set; }

        [DataMember(Name = "availableFrom")]
        public DateTime AvailableFrom { get; set; }

        [DataMember(Name = "availableTo")]
        public DateTime? AvailableTo { get; set; }

        [DataMember(Name = "isDisabled")]
        public bool IsDisabled { get; set; }

        [DataMember(Name = "url")]
        public string FriendlyUrl { get; set; }

        [DataMember(Name = "seoPageTitle")]
        public string SeoPageTitle { get; set; }

        [DataMember(Name = "seoPageDescription")]
        public string SeoPageDescription { get; set; }

        [DataMember(Name = "seoPageKeywords")]
        public string SeoPageKeywords { get; set; }

        [DataMember(Name = "codeTop")]
        public string CodeTop { get; set; }

        [DataMember(Name = "codeBottom")]
        public string CodeBottom { get; set; }

        [DataMember(Name = "displayInMenu")]
        public bool DisplayInMenu { get; set; }

        [DataMember(Name = "menuText")]
        public string MenuText { get; set; }

        [DataMember(Name = "redirectToUrl")]
        public string RedirectToUrl { get; set; }

        [DataMember(Name = "cssClass")]
        public string CssClass { get; set; }

        [DataMember(Name = "menuOrder")]
        public int MenuOrder { get; set; }

        [DataMember(Name = "templateHtml")]
        public string TemplateHtml { get; set; }

        [DataMember(Name = "children")]
        public PageModel[] Children { get; set; }

        [DataMember(Name = "updatedOn")]
        public DateTime UpdatedOn { get; set; }
    }
}