﻿using System;
using System.Runtime.Serialization;

namespace HotelWebsite.Models.Pages
{
    [DataContract]
    public class TemplateModel
    {
        [DataMember(Name = "id")]
        public Guid Id { get; set; }

        [DataMember(Name = "name")]
        public string Name { get; set; }

        [DataMember(Name = "templateLocation")]
        public string TemplateLocation { get; set; }
    }
}