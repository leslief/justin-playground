﻿using System.Globalization;
using DevTrends.MvcDonutCaching;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace HotelWebsite.Controllers
{
    public class PagesController : Controller
    {
        public ActionResult Index() 
        {
            return RedirectPermanent("/");
        }

        [DonutOutputCache(CacheProfile = "StandardCache")]
        public ActionResult Sitemap()
        {
            var items = MarcusApps.Api.Pages.GetTreeMenu("/", 4);
         
            return View(items);
        }

        public ActionResult SitemapXml()
        {
            XNamespace ns = "http://www.sitemaps.org/schemas/sitemap/0.9";

            var urlSet = new XDocument(
            new XElement(ns + "urlset",
                         new XAttribute("xmlns", ns.NamespaceName),
                         new XAttribute("fileName", "sitemap.xml")
                         ));

            var items = MarcusApps.Api.Pages.GetAll()
                .Where(x=>
                    ((x.AvailableTo == null && x.AvailableFrom <= DateTime.Now) || (x.AvailableTo >= DateTime.Now && x.AvailableFrom <= DateTime.Now))
                    && !x.IsDisabled 
                    //&& x.DisplayInMenu
                );
            foreach (var item in items)
            {
                var url = new XElement(ns + "url",
                    new XElement(ns + "loc", item.FriendlyUrl),
                    new XElement(ns + "lastmod", DateTime.Now.ToString(CultureInfo.InvariantCulture)),
                    new XElement(ns + "changefreq", "monthly"),
                    new XElement(ns + "priority", "0.8")
                );
                var xElement = urlSet.Element(ns + "urlset");
                
                if (xElement != null) xElement.Add(url);
            }


            return Content(urlSet.ToString(), "text/html");
        }

        [DonutOutputCache(CacheProfile = "PageCache")]
        public ActionResult View(Guid id)
        {
            var page = MarcusApps.Api.Pages.GetById(id, false, true);
            if (page != null)
            {
                if (!String.IsNullOrEmpty(page.RedirectToUrl)) return RedirectPermanent(page.RedirectToUrl);
                return Content(ParseCode(page.RenderedContent));
            }

            throw new HttpException(404, "NotFound");
        }

        public ActionResult AreaAttractions()
        {
            return View("~/views/shared/widgets/_AreaAttractions.cshtml");
        }

        public ActionResult Error404()
        {
            return View();
        }

        public string ParseCode(string input)
        {
            const string pattern = @"%%([^}]+)%%";
            var matches = Regex.Matches(input, pattern, RegexOptions.IgnoreCase);

            foreach (Match match in matches)
            {
                foreach (Capture capture in match.Captures)
                {
                    try
                    {
                        string token;
                        var val = "";

                        if (capture.Value.Contains(':'))
                        {
                            token = capture.Value.Split(':')[0].Replace("%%", "");
                            val = capture.Value.Split(':')[1].Replace("%%", "");
                        }
                        else
                        {
                            token = capture.Value.Replace("%%", "");
                        }

                        var replace = capture.Value;

                        switch (token.ToLower())
                        {
                            case "packagebanner":
                                replace = LoadContentFromUrl("/packages/banner?categoryName=" + val);
                                break;
                            case "areaattractions":
                                replace = LoadContentFromUrl("/pages/areaattractions");
                                break;
                            case "album":
                                replace = LoadContentFromUrl("/media/albumwidget?album=" + val);
                                break;
                        }
                        input = input.Replace(capture.Value, replace);
                        //var x = String.Format("Index={0}, Token={1}, Value={2}", capture.Index, token, val);
                        //Response.Write(x);
                    }
                    catch { }
                }
            }

            return input;
        }

        public string LoadContentFromUrl(string url)
        {
            var html = "";
            Uri uri = null;
            
            if (Request.Url != null) Uri.TryCreate(Request.Url.AbsoluteUri, UriKind.Absolute, out uri);
            
            if (uri != null)
            {
                var rawUrl = uri.Scheme + "://" + uri.Host + (!uri.IsDefaultPort ? ":" + uri.Port : "");

                var request = WebRequest.Create(rawUrl + url);
                request.Credentials = CredentialCache.DefaultCredentials;

                var response = request.GetResponse();

                // the response was 'OK' so we can start to process it
                if (((HttpWebResponse)response).StatusDescription == "OK")
                {

                    var dataStream = response.GetResponseStream();
                    if (dataStream != null)
                    {
                        var reader = new StreamReader(dataStream);
                        html = reader.ReadToEnd();

                        reader.Close();
                    }
                }

                response.Close();
            }

            return html;
        }
    }
}

