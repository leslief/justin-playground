﻿using System.Web.Mvc;
using DevTrends.MvcDonutCaching;

namespace HotelWebsite.Controllers
{
    public class CacheController : Controller
    {
        public ActionResult ClearCache(string c = "", string a = "")
        {
            var cacheManager = new OutputCacheManager();

            if (string.IsNullOrEmpty(c) && string.IsNullOrEmpty(a))
            {
                //remove all cached action outputs
                cacheManager.RemoveItems();

                //redirect to home/index
                return RedirectToAction("Index", "Home", null);
            }

            if (!string.IsNullOrEmpty(c) && !string.IsNullOrEmpty(a))
            {
                //remove all cached action outputs
                cacheManager.RemoveItems(c, a); 

                //redirect to home/index
                return RedirectToAction(a, c, null);
            }

            if (!string.IsNullOrEmpty(c) && string.IsNullOrEmpty(a))
            {
                //remove all cached action outputs
                cacheManager.RemoveItems(c);

                //redirect to home/index
                return RedirectToAction("Index", c, null);
            }

            //redirect to home/index
            return RedirectToAction("Index", "Home", null);
        }
    }
}
