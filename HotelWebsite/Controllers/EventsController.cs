﻿using DevTrends.MvcDonutCaching;
using System;
using System.Linq;
using System.Web.Mvc;
using HotelWebsite.Models.Events;

namespace HotelWebsite.Controllers
{
    public class EventsController : Controller
    {
        [DonutOutputCache(CacheProfile = "StandardCache")]
        public ActionResult Index(string startDate, string endDate)
        {
            return View();
        }

        [DonutOutputCache(CacheProfile = "StandardCache")]
        public ActionResult Community(string startDate, string endDate)
        {
            return View();
        }

        [DonutOutputCache(CacheProfile = "StandardCache")]
        public ActionResult Details(Guid id)
        {
            var evt = MarcusApps.Api.Events.Get(id) ?? new EventModel();

            return View(evt);
        }

        public PartialViewResult Listing(string startDate, string endDate)
        {
            var model = new EventListingModel
            {
                StartDate = startDate,
                EndDate = endDate,
                Events = MarcusApps.Api.Events.Get(future: true, start: startDate, end: endDate),
                RecurringEvents = MarcusApps.Api.Events.GetRecurring()
            };

            if (model.RecurringEvents != null)
            {
                var events = model.Events.ToList();
                events.InsertRange(0, model.RecurringEvents);
                model.Events = events.OrderBy(x => x.StartOn);
            }

            return PartialView("_Listing", model);
        }

    }
}
