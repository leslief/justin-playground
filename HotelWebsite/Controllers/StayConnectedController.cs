﻿using System;
using System.Configuration;
using System.Web.Mvc;
using HotelWebsite.OAuth.Twitter.Services;
using HotelWebsite.OAuth.Twitter.Services.Implamentations;

namespace HotelWebsite.Controllers
{
    public class StayConnectedController : Controller
    {
        private readonly IOAuthTwitterWrapper _oAuthTwitterWrapper;

        public StayConnectedController(IOAuthTwitterWrapper oAuthTwitterWrapper)
        {
            _oAuthTwitterWrapper = oAuthTwitterWrapper;
        }

        public ActionResult Index()
        {
            return View();
        }

        public string GetTwitterFeed(string _ = "",
                                        string url = "timeline",                                
                                        string q = "",
                                        string count = "5",
                                        string include_rts = "false",
                                        string exclude_replies = "true",
                                        string screen_name = "",
                                        string list_id = "",
                                        string owner = ""                                            
            )
        {
            int tweetCount;
            bool includeRts;
            bool excludeReplies;
            
            var r1 = int.TryParse(count, out tweetCount);
            var r2 = bool.TryParse(include_rts, out includeRts);
            var r3 = bool.TryParse(exclude_replies, out excludeReplies);

            switch (url)
            {
                case "timeline":
                    var timeLineSettings = new TimeLineSettings
                    {
                        ScreenName = ConfigurationManager.AppSettings["screenname"],
                        IncludeRts = r2 ? includeRts.ToString() : ConfigurationManager.AppSettings["include_rts"],
                        ExcludeReplies = r3 ? excludeReplies.ToString() : ConfigurationManager.AppSettings["exclude_replies"],
                        Count = r1 ? tweetCount : Convert.ToInt16(ConfigurationManager.AppSettings["count"]),
                        TimelineFormat = ConfigurationManager.AppSettings["timelineFormat"] 
                    };

                    _oAuthTwitterWrapper.SetTimeLineSettings(timeLineSettings);

                    return _oAuthTwitterWrapper.GetMyTimeline();

                case "search":
                    var searchFormat = ConfigurationManager.AppSettings["searchFormat"];
                    var searchQuery = ConfigurationManager.AppSettings["searchQuery"];

                    var searchSettings = new SearchSettings
                    {
                        SearchFormat = searchFormat,
                        SearchQuery = searchQuery
                    };

                    _oAuthTwitterWrapper.SetSearchSetting(searchSettings);

                    return _oAuthTwitterWrapper.GetSearch();
                default :
                    return "Unsupported Feature.";
            }
        }
    }
}

