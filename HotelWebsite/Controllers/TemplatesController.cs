﻿using System;
using System.Web.Mvc;

namespace HotelWebsite.Controllers
{
    public class TemplatesController : Controller
    {
        //[DonutOutputCache(CacheProfile = "PageCache")]
        public ActionResult Render(string id = "", string pageId = "")
        {
            if (!String.IsNullOrEmpty(pageId))
            {
                var page = MarcusApps.Api.Pages.GetById(new Guid(pageId));

                ViewBag.Title = !String.IsNullOrWhiteSpace(page.SeoPageTitle) ? page.SeoPageTitle : page.PageName;
                ViewBag.SeoPageKeywords = page.SeoPageKeywords;
                ViewBag.SeoPageDescription = page.SeoPageDescription;
                ViewBag.PageId = pageId;
            }                

            var templateLocation = String.Format("~/views/shared/templates/{0}.cshtml", id);
            return View(templateLocation);
        }
    }
}
