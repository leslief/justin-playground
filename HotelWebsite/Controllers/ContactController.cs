﻿using System;
using System.Web.Mvc;
using System.Data.SqlClient;
using HotelWebsite.Models.Forms;

namespace HotelWebsite.Controllers
{
    public class ContactController : Controller
    {
        private const string ConnString = "Data Source=mhs-mssql1-aws; Initial Catalog=Web01; Uid=web01user; Pwd=m@rcu$@pp$; MultipleActiveResultSets=true";

        public ActionResult EmailSignUp()
        {
            var model = new ContactModel
                {
                    EmailAddress = Request["emailAddress"]
                };
            return View(model);
        }

        [HttpPost]
        public ActionResult EmailSignUp(ContactModel model)
        {
            SignUpByLid(00, model);

            //if (model.Spa) SignUpByLid(59,model);
            //if (model.Dining) SignUpByLID(13,model);
            //if (model.Holidays) SignUpByLID(15,model);

            return RedirectToAction("EmailSignUpSuccess");
        }

        public void SignUpByLid(int lid, ContactModel model) {

            using (var conn = new SqlConnection(ConnString))
            {
                conn.Open();

                var cmd = new SqlCommand(String.Format("exec sp_EML_SignUps '{0}', '{1}'", lid, model.EmailAddress), conn);
                var reader = cmd.ExecuteReader();

                if (!reader.HasRows)
                {

                    var cmdInsert = new SqlCommand(String.Format("exec sp_EML_InsertEmailSignUp '{0}', '{1}', '{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}'",
                        lid,
                        "BaseHotel",
                        model.FirstName,
                        model.LastName,
                        model.EmailAddress,
                        model.Address1,
                        model.Address2,
                        model.City,
                        model.State,
                        model.ZipCode,
                        model.BirthdayMonth,
                        model.BirthdayDay,
                        model.AnniversaryMonth,
                        model.AnniversaryDay,
                        model.Comments,
                        ""
                        ), conn);

                    cmdInsert.ExecuteNonQuery();
                }
            }
        }

        public ActionResult EmailSignUpSuccess()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EmailUnsubscribe(string EmailAddress)
        {
            UnsubscribeByLid(00, EmailAddress);

            return RedirectToAction("EmailUnsubscribeSuccess");
        }

        public void UnsubscribeByLid(int lid, string email)
        {

            using (var conn = new SqlConnection(ConnString))
            {
                conn.Open();

                var cmd = new SqlCommand(String.Format("exec sp_EML_SunsubscribeEmail '{0}', '{1}'",
                    lid,
                    email
                    ), conn);

                cmd.ExecuteNonQuery();
            }
        }
        public ActionResult ContactUs()
        {
            var model = new ContactModel(); 
            return View(model);

        }

        public ActionResult ContactUsSuccess()
        {
            return View();
        }
    }
}
