﻿using DevTrends.MvcDonutCaching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HotelWebsite.Models.Pages;

namespace HotelWebsite.Controllers
{
    public class MenuController : Controller
    {
        [DonutOutputCache(CacheProfile = "StandardCache")]
        public ActionResult Main()
        {
            var items = MarcusApps.Api.Pages.GetTreeMenu("/", 3);

            return PartialView(items);
        }

        [DonutOutputCache(CacheProfile = "StandardCache")]
        public ActionResult Breadcrumbs(string breadcrumbPageId = "", string page = "")
        {
            if (!String.IsNullOrEmpty(breadcrumbPageId))
            {
                var id = new Guid(breadcrumbPageId);
                var items = MarcusApps.Api.Pages.GetBreadcrumbsByPageId(id);
                return PartialView(items);
            }
            
            // see if we can get breadcrumbs based on controllers/action
            try
            {
                var rd = ControllerContext.ParentActionViewContext.RouteData;                   
                var currentController = rd.GetRequiredString("controller");

                var items = new List<PageModel>
                    {
                        new PageModel {PageName = "Home", FriendlyUrl = "/"},
                        new PageModel {PageName = currentController, FriendlyUrl = "/" + currentController}
                    };

                return PartialView(items);
            }
            catch
            {

            }

            return Content("");
        }

        [DonutOutputCache(CacheProfile = "StandardCache")]
        public ActionResult SideNav(string sideNavPageId = "")
        {
            if (!String.IsNullOrEmpty(sideNavPageId))
            {
                var topLevelParent = MarcusApps.Api.Pages.GetTopLevelParentByChildPageId(new Guid(sideNavPageId));

                var items = topLevelParent.ParentPageId == null
                                 ? MarcusApps.Api.Pages.GetTreeMenu(new Guid(sideNavPageId), 2).ToList()
                                 : MarcusApps.Api.Pages.GetTreeMenu(topLevelParent.Id, 2).ToList();

                ViewBag.PageId = sideNavPageId;

                return PartialView(items);
            }

            return Content("");
        }
    }
}
