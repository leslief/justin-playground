﻿using DevTrends.MvcDonutCaching;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using HotelWebsite.Models.Packages;

namespace HotelWebsite.Controllers
{
    public class PackagesController : Controller
    {
        [DonutOutputCache(CacheProfile = "StandardCache")]
        public ActionResult Index()
        {
            return View();
        }

        [DonutOutputCache(CacheProfile = "StandardCache")]
        public ActionResult Details(Guid id)
        {
            var package = MarcusApps.Api.Packages.Get(id) ?? new PackageModel();

            return View(package);
        }

        [DonutOutputCache(CacheProfile = "StandardCache")]
        public PartialViewResult Banner(string categoryName)
        {
            var packages = MarcusApps.Api.Packages.GetByCategoryName(categoryName);
            
            return PartialView(packages);
        }

        public PartialViewResult Listing()
        {
            var model = new PackageListingModel
            {
                Categories = new List<CategoryModel>(),
                Packages = new List<PackageModel>()
            };

            var packages = MarcusApps.Api.Packages.GetAll().Where(x => !x.IsHidden);
            var packageModels = packages as PackageModel[] ?? packages.ToArray();

            model.Packages = packageModels;

            var categories = new List<CategoryModel>();

            foreach (var package in packageModels.Where(package => package.Categories.Any()))
            {
                categories.AddRange(package.Categories.Where(x => !x.IsDisabled));
            }

            model.Categories = categories.Distinct().OrderBy(x => x.Name);


            return PartialView("_Listing", model);
        }

    }
}
