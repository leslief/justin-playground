﻿using System.Web.Mvc;

namespace HotelWebsite.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        // Landing Page
        public ActionResult Index()
        {
            return View();
        }
      
        //Interior with Aside and Header Image
        public ActionResult Interior()
        {
            return View();
        }

        //Interior with Aside and No Header Image
        public ActionResult InteriorNoHeader() 
        {
            return View();
        }
        
        //Interior One Column and Header Image
        public ActionResult InteriorOneColumn() 
        {
            return View();
        }

        //Interior One Column and No Header Image
        public ActionResult InteriorOneColNoHeader()
        {
            return View();
        }
    }
}
