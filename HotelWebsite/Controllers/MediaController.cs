﻿using System.Collections.Generic;
using DevTrends.MvcDonutCaching;
using System;
using System.Linq;
using System.Web.Mvc;
using HotelWebsite.Models.Media;

namespace HotelWebsite.Controllers
{
    public class MediaController : Controller
    {
        [DonutOutputCache(CacheProfile = "StandardCache")]
        public ActionResult Index()
        {
            var model = MarcusApps.Api.PhotoGallery.GetAlbums();

            return View(model);
        }

        public ActionResult Video()
        {
            return View();
        }

        [DonutOutputCache(CacheProfile = "StandardCache")]
        public ActionResult Album(Guid id, Guid? photo)
        {
            var photos = MarcusApps.Api.PhotoGallery.GetAlbumPhotos(id);

            var photoModels = photos as IList<PhotoModel> ?? photos.ToList();

            if (photos != null && photoModels.Any())
            {
                var firstOrDefault = photoModels.FirstOrDefault();
                if (firstOrDefault != null)
                {
                    var model = firstOrDefault.Album;

                    model.Photos = photoModels;
                    model.StartingIndex = 0;
                    model.StartingIndex = photoModels.ToList().FindIndex(x => x.Id == photo);
                    
                    if (model.StartingIndex < 0) model.StartingIndex = 0;

                    return View(model);
                }
            }
            return View();
        }

        [DonutOutputCache(CacheProfile = "StandardCache")]
        public PartialViewResult AlbumWidget(string album)
        {
            var albums = MarcusApps.Api.PhotoGallery.GetAlbums(true).Where(x=>x.Name.ToLower() == album.ToLower());

            var albumModels = albums as IList<AlbumModel> ?? albums.ToList();

            if (albumModels.Any())
            {                
                var model = albumModels.FirstOrDefault();
                if (model != null)
                {
                    model.Photos = MarcusApps.Api.PhotoGallery.GetAlbumPhotos(model.Id);
                    return PartialView(model);
                }
            }
            return PartialView();
        }
    }
}
