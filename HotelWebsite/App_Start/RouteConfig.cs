﻿using System.Web.Mvc;
using System.Web.Routing;

namespace HotelWebsite
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            #region CUSTOM URLS FOR STATIC PAGES/CONTROLLERS

            routes.MapRoute(
                name: "Packages",
                url: "wisconsin-getaway/resort-packages",
                defaults: new { controller = "packages", action = "index" }
            );

            routes.MapRoute(
                name: "Events",
                url: "wisconsin-weekend-getaway/resort-events",
                defaults: new { controller = "events", action = "index" }
            );

            routes.MapRoute(
                name: "Contact Us",
                url: "green-lake-resort/contact-heidel-house",
                defaults: new { controller = "contact", action = "contactus" }
            );

            routes.MapRoute(
                name: "Email Subscribe",
                url: "email-subscribe",
                defaults: new { controller = "contact", action = "emailsignup" }
            );

            routes.MapRoute(
                name: "Photo Gallery",
                url: "green-lake-resort/heidel-house-images",
                defaults: new { controller = "media", action = "index" }
            );

            routes.MapRoute(
                name: "Video Gallery",
                url: "green-lake-resort/heidel-house-videos",
                defaults: new { controller = "media", action = "video" }
            );

            routes.MapRoute(
                name: "Cache",
                url: "{controller}/{action}/{c}/{a}",
                defaults: new { controller = "Cache", action = "ClearCache", c = "", a = "" }
            );

            #endregion

            routes.MapRoute(
                name: "Sitemap",
                url: "sitemap.xml",
                defaults: new { controller = "Pages", action = "SitemapXml" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            ).RouteHandler = new MarcusApps.PageRouteHandler(); ;



        }
    }
}