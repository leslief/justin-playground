﻿using System.Web.Optimization;

namespace HotelWebsite
{
    public class BundleConfig
    {
        //to use: @Scripts.Render("~/bundles/jquery")
        //        @Styles.Render("~/Content/themes/base/css")
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.UseCdn = true;

            #region Scripts


            //bundles.Add(new ScriptBundle("~/assets/js/jquery").Include(
            //    "~/Scripts/jquery-{version}.min.js",
            //    "~/Scripts/jquery-migrate-{version}.min.js"));
            bundles.Add(new ScriptBundle("~/Content/themes/base/slider").Include(
                "~/Scripts/vendor/tweenlite.js",
                "~/Scripts/vendor/jquery.easing-1.3.pack.js",
                "~/Scripts/vendor/jquery.stellar.js",
                "~/Scripts/vendor/mightyslider.js"
                ));

            bundles.Add(new ScriptBundle("~/Content/themes/base/js").Include(
                "~/Scripts/vendor/enquire.js",
                "~/Scripts/imageHandler.js",
                "~/Scripts/vendor/jquery.sticky.js",
                "~/Scripts/lazyload.js",
                "~/Scripts/vendor/jquery.colorbox-min.js",
                "~/Scripts/vendor/placeholder.js",
                "~/Scripts/imagesLoaded.js",
                "~/Scripts/vendor/isotope.pkgd.min.js",
                "~/Scripts/vendor/packery-mode.pkgd.min.js",
                "~/Scripts/jquery-ui.js",
                "~/Scripts/plugins.js",
                "~/Scripts/main.js",
                "~/Scripts/MarcusApps/PackageBanner.js",
                "~/Scripts/vendor/jquery.scrollUp.js",
                "~/Scripts/vendor/slide-navigation.js",
                "~/Scripts/vendor/parsley.js"
                ));
            #endregion

            #region Styles

            //bundles.Add(new StyleBundle("~/assets/fonts", "https://fonts.googleapis.com/css?family=Telex"));
            //bundles.Add(new StyleBundle("~/assets/css/pfister").Include(
            //    "~/Content/css/MarcusApps.css",
            //    "~/Content/css/form.css"));
            
            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                "~/Content/css/normalize.css",
                "~/Content/css/main.css",
                "~/Content/css/colorbox.css",
                "~/Content/css/jquery-ui.css",
                "~/Content/css/jquery-ui.theme.css"
                ));
           
            //bundles.Add(new Bundle("~/assets/css/apps").Include("~/Content/css/apps/*.css"));
            BundleTable.EnableOptimizations = true;

            BundleTable.Bundles.IgnoreList.Clear(); // apparently, IgnoreList included .min.js in debug
            //BundleTable.Bundles.IgnoreList.Ignore(".intellisense.js", OptimizationMode.Always);
            //BundleTable.Bundles.IgnoreList.Ignore("-vsdoc.js", OptimizationMode.Always);
            //BundleTable.Bundles.IgnoreList.Ignore(".debug.js", OptimizationMode.Always);
            //BundleTable.Bundles.IgnoreList.Ignore(".remote.js", OptimizationMode.Always);
            
            #endregion
        }
    }
}