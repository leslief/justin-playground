﻿using System.Globalization;
using HotelWebsite.Models.Events;
using HotelWebsite.Models.Media;
using HotelWebsite.Models.Packages;
using HotelWebsite.Models.Pages;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Configuration;

namespace HotelWebsite.MarcusApps
{
    public class Api
    {
        public const string BaseUrl = "https://api.marcusapps.com";
        public static string SiteKey = ConfigurationManager.AppSettings["SiteKey"];

        public class Events
        {
            private const string UrlType = "Events";

            public static IEnumerable<EventModel> Get(
                Guid? categoryId = null,
                int? eventLimit = null,
                int? dayLimit = null,
                bool future = false,
                string start = "",
                string end = "",
                bool includeRecurring = false)
            {
                if (!String.IsNullOrEmpty(start))
                    start =
                        Helper.DateTimeToUnixTimestamp(Convert.ToDateTime(start)).ToString(CultureInfo.InvariantCulture);
                if (!String.IsNullOrEmpty(end))
                    end = Helper.DateTimeToUnixTimestamp(Convert.ToDateTime(end)).ToString(CultureInfo.InvariantCulture);

                var objs = Helper.Get(UrlType, "Events", "Get",
                                      new
                                      {
                                          siteKey = SiteKey,
                                          categoryId,
                                          eventLimit,
                                          dayLimit,
                                          future,
                                          start,
                                          end,
                                          includeRecurring
                                      });

                return Helper.JsonToList<EventModel>(objs);
            }

            public static IEnumerable<EventModel> GetRecurring()
            {
                var objs = Helper.Get(UrlType, "RecurringEvents", "GetRecurringEventsByWebsite", new { siteKey = SiteKey });
                return Helper.JsonToList<EventModel>(objs);
            }

            public static EventModel Get(Guid id)
            {
                var obj = Helper.Get(UrlType, "Events", "Get", new { id });
                return Helper.JsonToObject<EventModel>(obj);
            }
        }

        public class Packages
        {
            private const string UrlType = "Packages";

            public static IEnumerable<PackageModel> GetAll()
            {
                var objs = Helper.Get(UrlType, "Packages", "GetAll", new { siteKey = SiteKey });
                return Helper.JsonToList<PackageModel>(objs);
            }

            public static IEnumerable<PackageModel> GetByCategoryName(string categoryName = "")
            {
                var objs = Helper.Get(UrlType, "Packages", "GetByCategoryName", new { siteKey = SiteKey, categoryName });
                return Helper.JsonToList<PackageModel>(objs);
            }

            public static IEnumerable<String> GetCategories()
            {
                var objs = Helper.Get(UrlType, "Packages", "GetCategories", new { siteKey = SiteKey });
                return Helper.JsonToList<String>(objs);
            }

            public static PackageModel Get(Guid id)
            {
                var obj = Helper.Get(UrlType, "Packages", "GetById", new { siteKey = SiteKey, id });
                return Helper.JsonToObject<PackageModel>(obj);
            }
        }

        public class Pages
        {
            private const string UrlType = "Pages";

            public static IEnumerable<PageModel> GetAll()
            {
                var objs = Helper.Get(UrlType, "Pages", "GetAll", new { siteKey = SiteKey });
                return Helper.JsonToList<PageModel>(objs);
            }

            public static IEnumerable<PageModel> GetTreeMenu(string baseUrl = null, int numberOfLevels = 1, int currentLevel = 1)
            {
                var obj = Helper.Get(UrlType, "Pages", "GetTreeMenu", new { siteKey = SiteKey, baseUrl, numberOfLevels, currentLevel });
                return Helper.JsonToList<PageModel>(obj);
            }

            public static IEnumerable<PageModel> GetTreeMenu(Guid? baseGuid = null, int numberOfLevels = 1, int currentLevel = 1)
            {
                var obj = Helper.Get(UrlType, "Pages", "GetTreeMenu", new { siteKey = SiteKey, baseGuid, numberOfLevels, currentLevel });
                return Helper.JsonToList<PageModel>(obj);
            }

            public static PageModel GetById(Guid id, bool getChildren = false, bool useLocalTemplate = false)
            {
                var templateHtmlOverride = "";

                if (useLocalTemplate)
                {
                    templateHtmlOverride = GetLocalTemplateHtmlByPageId(id);
                }

                var obj = Helper.Post(UrlType, "Pages", "Post", new { siteKey = SiteKey, id, getChildren, templateHtmlOverride });
                return Helper.JsonToObject<PageModel>(obj);
            }

            public static PageModel GetByFriendlyUrl(string friendlyUrl)
            {
                var obj = Helper.Get(UrlType, "Pages", "GetByFriendlyUrl", new { siteKey = SiteKey, friendlyUrl });
                return Helper.JsonToObject<PageModel>(obj);
            }

            public static IEnumerable<PageModel> GetBreadcrumbsByPageId(Guid id)
            {
                var obj = Helper.Get(UrlType, "Pages", "GetBreadcrumbsByPageId", new { siteKey = SiteKey, id });
                return Helper.JsonToList<PageModel>(obj);
            }

            public static PageModel GetTopLevelParentByChildPageId(Guid id)
            {
                var obj = Helper.Get(UrlType, "Pages", "GetTopLevelParentByChildPageId", new { siteKey = SiteKey, id });
                return Helper.JsonToObject<PageModel>(obj);
            }

            public static string GetLocalTemplateHtmlByPageId(Guid id)
            {
                var obj = Helper.Get(UrlType, "Pages", "GetTemplateUrlByPageId", new { siteKey = SiteKey, id });
                var html = "";

                if (obj != null)
                {
                    var serializer = new JavaScriptSerializer();
                    var templateUrl = serializer.Deserialize(obj, typeof(String)).ToString();

                    Uri uri;
                    Uri.TryCreate(HttpContext.Current.Request.Url.AbsoluteUri, UriKind.Absolute, out uri);
                    var rawUrl = uri.Scheme + "://" + uri.Host + (!uri.IsDefaultPort ? ":" + uri.Port : "");

                    var request = WebRequest.Create(rawUrl + templateUrl + "?pageId=" + id);
                    request.Credentials = CredentialCache.DefaultCredentials;

                    var response = request.GetResponse();

                    // the response was 'OK' so we can start to process it
                    if (((HttpWebResponse)response).StatusDescription == "OK")
                    {

                        var dataStream = response.GetResponseStream();
                        if (dataStream != null)
                        {
                            var reader = new StreamReader(dataStream);
                            html = reader.ReadToEnd();

                            reader.Close();
                        }
                    }

                    response.Close();
                }

                return html;
            }
        }

        public class PhotoGallery
        {
            private const string UrlType = "Photos";

            public static IEnumerable<AlbumModel> GetAlbums(bool allowDisabled = false)
            {
                var objs = Helper.Get(UrlType, "Albums", "Get", new { siteKey = SiteKey, allowDisabled });
                return Helper.JsonToList<AlbumModel>(objs);
            }

            public static IEnumerable<PhotoModel> GetAllPhotos()
            {
                var objs = Helper.Get(UrlType, "Photos", "Get", new { siteKey = SiteKey });
                return Helper.JsonToList<PhotoModel>(objs);
            }

            public static IEnumerable<PhotoModel> GetAlbumPhotos(Guid albumId)
            {
                var objs = Helper.Get(UrlType, "Photos", "Get", new { albumId });
                return Helper.JsonToList<PhotoModel>(objs);
            }
        }

        public class Helper
        {
            public static string Post(string type, string controller, string action, object obj)
            {
                var request = WebRequest.Create(BaseUrl + "/" + type + "/" + controller + "/" + action);
                //var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:58866/pages/post");

                request.Method = "POST";

                var serializer = new JavaScriptSerializer();
                var postData = serializer.Serialize(obj);
                var byteArray = Encoding.UTF8.GetBytes(postData);

                request.ContentType = "application/json";
                request.ContentLength = byteArray.Length;

                var dataStream = request.GetRequestStream();
                dataStream.Write(byteArray, 0, byteArray.Length);
                dataStream.Close();

                var response = request.GetResponse();
                Console.WriteLine(((HttpWebResponse)response).StatusDescription);
                dataStream = response.GetResponseStream();

                if (dataStream != null)
                {
                    var reader = new StreamReader(dataStream);
                    var responseFromServer = reader.ReadToEnd();

                    reader.Close();
                    dataStream.Close();
                    response.Close();

                    return responseFromServer;
                }

                return null;
            }

            public static string Post_(string type, string controller, string action, object obj)
            {
                var httpWebRequest = WebRequest.Create(BaseUrl + "/" + type + "/" + controller + "Post" + "/" + action);
                //var httpWebRequest = (HttpWebRequest)WebRequest.Create("http://localhost:58866/pages/post");

                var responseFromServer = "";
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";

                var json = JsonConvert.SerializeObject(obj);

                try
                {
                    using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                    {
                        streamWriter.Write(json);
                    }

                    using (var stream = httpWebRequest.GetResponse().GetResponseStream())
                    {
                        if (stream != null)
                            using (var reader = new StreamReader(stream))
                            {
                                responseFromServer = reader.ReadToEnd();
                            }
                    }
                }
                catch (WebException)
                {
                    responseFromServer = "error";
                }

                return responseFromServer;
            }

            public static string Get(string type, string controller, string action, object obj)
            {
                var request =
                    WebRequest.Create(BaseUrl + "/" + type + "/" + controller + "/" + action + "?" +
                                      obj.GetQueryString());
                //WebRequest request = WebRequest.Create("http://localhost:58866" + "/" + type + "/" + controller + "/" + action + "?" + obj.GetQueryString());

                request.Method = "GET";
                var responseFromServer = "";

                try
                {
                    using (var dataStream = request.GetResponse().GetResponseStream())
                    {
                        if (dataStream != null)
                            using (var reader = new StreamReader(dataStream))
                            {
                                responseFromServer = reader.ReadToEnd();
                            }
                    }
                }
                catch (WebException)
                {
                    responseFromServer = "error";
                }
                return responseFromServer;
            }

            public static T JsonToObject<T>(string json)
            {
                if (json == "error") return default(T);

                var jObj = JObject.Parse(json);
                var obj = jObj.ToObject<T>();
                return obj;
            }

            public static List<T> JsonToList<T>(string json)
            {
                if (json == "error") return null;

                var obj = JArray.Parse(json);
                var list = obj.ToObject<List<T>>();
                return list;
            }

            public static string JsonToDecodedString(string json)
            {
                if (json == "error") return null;

                var obj = JObject.Parse(json);
                return obj[0].ToString();
            }

            public static double DateTimeToUnixTimestamp(DateTime dateTime)
            {
                return (dateTime - new DateTime(1970, 1, 1).ToLocalTime()).TotalSeconds;
            }
        }
    }

    public static class ExtensionMethods
    {
        public static string GetQueryString(this object obj)
        {
            var properties = from p in obj.GetType().GetProperties()
                             where p.GetValue(obj, null) != null
                             select p.Name + "=" + HttpUtility.UrlEncode(p.GetValue(obj, null).ToString());

            return String.Join("&", properties.ToArray());
        }
    }
}