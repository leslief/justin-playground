﻿using HotelWebsite.Models.Pages;

namespace HotelWebsite.MarcusApps
{
    public static class RedirectManager
    {
        public static PageModel GetPageByFriendlyUrl(string friendlyUrl)
        {
            var page = Api.Pages.GetByFriendlyUrl(friendlyUrl);
            return page;
        }
    }
}