﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HotelWebsite.MarcusApps
{
    public class PageRouteHandler : MvcRouteHandler
    {
        protected override IHttpHandler GetHttpHandler(RequestContext requestContext)
        {
            var url = requestContext.HttpContext.Request.Path;

            if (!string.IsNullOrEmpty(url))
            {
                var page = RedirectManager.GetPageByFriendlyUrl(url);
                if (page != null)
                {
                    FillRequest("pages", "view", page.Id.ToString(), requestContext);
                }
            }

            return base.GetHttpHandler(requestContext);
        }

        private static void FillRequest(string controller, string action, string id, RequestContext requestContext)
        {
            if (requestContext == null)
            {
                throw new ArgumentNullException("requestContext");
            }

            requestContext.RouteData.Values["controller"] = controller;
            requestContext.RouteData.Values["action"] = action;
            requestContext.RouteData.Values["id"] = id;
        }
    }
    
}